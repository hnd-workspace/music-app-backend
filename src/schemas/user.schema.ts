import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsEmail } from "class-validator";
import { Document } from "mongoose";

export interface UserDTO {
  username: string;
  email: string;
  password: string;
  name: string;
  avatar?: string;
  verify: number;
}

@Schema({
  collection: "user",
})
export class UserModel implements UserDTO {
  @Prop({
    required: true,
  })
  username: string;

  @Prop()
  email: string;

  @Prop({
    required: true,
  })
  password: string;

  @Prop()
  name: string;

  @Prop()
  verify: number;
}

export type UserDocument = UserModel & Document;

export const UserSchema = SchemaFactory.createForClass(UserModel);
