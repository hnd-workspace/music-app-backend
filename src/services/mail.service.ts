import { MailerService } from "@nestjs-modules/mailer";
import { Injectable } from "@nestjs/common";

@Injectable()
export class MailService {
  constructor(private MailSvc: MailerService) {}

  async sendUserConfirmation(name: string, email: string, token: string) {
    const url = `http://localhost:3000/api/auth/confirm/${token}`;

    await this.MailSvc.sendMail({
      template: "confirmation",
      to: email,
      subject: "Welcome to Music App! Please confirm your Email!",
      context: {
        name,
        url,
      },
    });
  }

  async sendResetPassword(name: string, email: string, token: string) {
    const url = `http://localhost:3000/api/auth/reset-password/${token}`;

    await this.MailSvc.sendMail({
      template: "reset-password",
      to: email,
      subject: "Music App | Reset your password",
      context: {
        name,
        url,
      },
    });
  }

  async notificationResetPassword(name: string, email: string) {
    await this.MailSvc.sendMail({
      template: "reset-done",
      to: email,
      subject: "Music App | Your password changed !!!",
      context: {
        name,
        email,
      },
    });
  }
}
