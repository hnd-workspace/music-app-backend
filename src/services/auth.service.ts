import { Injectable } from "@nestjs/common";
import { UserService } from "./user.service";

@Injectable()
export class AuthService {
  constructor(private userSvc: UserService) {
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userSvc.getUserByUsername(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}