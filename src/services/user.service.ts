import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { UserDocument, UserDTO, UserModel } from "../schemas/user.schema";

@Injectable()
export class UserService {
  constructor(
    @InjectModel(UserModel.name) private userModel: Model<UserDocument>,
  ) {}

  getUsers(): Promise<UserModel[]> {
    return this.userModel.find().exec();
  }

  getUser(id: string): Promise<UserModel> {
    return this.userModel.findById(id).exec();
  }
  getUserByUsername(username: string): Promise<UserModel> {
    return this.userModel.findOne({ username }).exec();
  }
  getUserByEmail(email: string): Promise<UserModel> {
    return this.userModel.findOne({ email }).exec();
  }
  createUser(user: UserDTO): Promise<UserModel> {
    return this.userModel.create(user);
  }
  updateUser(id: string, user: UserDTO): Promise<UserModel> {
    return this.userModel
      .findByIdAndUpdate(id, user, {
        new: true,
        runValidators: true,
      })
      .exec();
  }
  uploadaAvatar(id: string, user: any): Promise<UserModel> {
    return this.userModel
      .findByIdAndUpdate(id, user, {
        new: true,
        runValidators: true,
      })
      .exec();
  }

  deleteUser(id): Promise<UserModel> {
    return this.userModel.findByIdAndDelete(id).exec();
  }
}
