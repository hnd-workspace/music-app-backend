import { join } from "path";
import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { HandlebarsAdapter } from "@nestjs-modules/mailer/dist/adapters/handlebars.adapter";
import { I18nJsonParser, I18nModule } from "nestjs-i18n";
import { MailerModule } from "@nestjs-modules/mailer";
import { MongooseModule } from "@nestjs/mongoose";

@Module({
  imports: [
    ConfigModule.forRoot(),
    I18nModule.forRoot({
      fallbackLanguage: "en",
      parser: I18nJsonParser,
      parserOptions: {
        path: join(__dirname, "../../i18n/"),
        watch: true
      }
    }),
    MongooseModule.forRoot(
      `mongodb+srv://${ process.env.DB_USERNAME }:${ process.env.DB_PASSWORD }@${ process.env.DB_HOST }/${ process.env.DB_NAME }?retryWrites=true&w=majority`
    ),
    MailerModule.forRoot({
      transport: {
        host: process.env["MAIL_HOST"],
        port: 587,
        secure: false,
        auth: {
          user: process.env["MAIL_USER"],
          pass: process.env["MAIL_PASSWORD"]
        }
      },
      defaults: {
        from: `\"No Reply\" ${ process.env["MAIL_FROM"] }`
      },

      template: {
        dir: join(__dirname, "../../views/mail-templates"),
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true
        }
      }
    })
  ],
  providers: []
})
export class SharedModule {
}
