import { ResponseInterface } from "../../../interfaces/response.interface";
import { MailService } from "../../../services/mail.service";
import { UserService } from "../../../services/user.service";
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UploadedFile,
  UseInterceptors
} from "@nestjs/common";
import { hashSync, compareSync } from "bcrypt";
import { sign } from "jsonwebtoken";
import { encode, decode } from "base-64";
import { FileInterceptor } from "@nestjs/platform-express";
import { Express } from "express";
import { diskStorage } from "multer";

@Controller()
export class AuthController {
  constructor(private userSvc: UserService, private mailSvc: MailService) {
  }

  @Post("/register")
  async register(@Body() payload: any): Promise<ResponseInterface<any>> {
    const { username, password, name, email } = payload;
    // validate pass

    // validate done
    const passHashed = hashSync(password, 12);
    // check unique
    const checkUser = await this.userSvc.getUserByUsername(username);
    let isCheckUser = false;
    if (checkUser) {
      if (checkUser.email == email || checkUser.username == username) {
        isCheckUser = true;
      }
    }
    //check done
    if (!isCheckUser) {
      const tokenEmail =
        Math.floor(1000 + Math.random() * 9000).toString() +
        encode("APP-MUSIC") +
        "." +
        encode(username);
      try {
        await this.mailSvc.sendUserConfirmation(name, email, tokenEmail);
        await this.userSvc.createUser({
          name,
          email,
          username,
          password: passHashed,
          verify: 0
        });
        return {
          data: {
            token: tokenEmail
          }
        };
      } catch (e) {
        return {
          errors: [ e ]
        };
      }
    } else {
      return {
        data: {
          msg: "Account already exists !!!"
        }
      };
    }
  }

  @Get("/confirm/:token")
  async confirm(@Param() params: any): Promise<ResponseInterface<any>> {
    const isUsername = decode(params.token.split(".").pop());
    const userDB: any = await this.userSvc.getUserByUsername(isUsername);
    if (userDB.verify == 0) {
      var verify = 1;
      const userUpdate: any = {
        verify
      };
      await this.userSvc.updateUser(userDB._id, userUpdate);
      return {
        data: {
          code: 200,
          msg: "Mail confirm success !!!"
        }
      };
    } else {
      return {
        data: {
          code: 200,
          msg: "Verified Mail !!!"
        }
      };
    }
  }

  @Post("/login")
  async login(@Body() payload: any): Promise<ResponseInterface<any>> {
    const { username, password } = payload;
    const userDB = await this.userSvc.getUserByUsername(username);
    const isPass = compareSync(password, userDB.password);
    const isVerify = userDB.verify > 0;
    if (isPass && isVerify) {
      const jwt = sign({ userDB }, process.env.JWT_KEY);
      return {
        data: {
          token: jwt
        }
      };
    } else {
      return {
        errors: "Login fails and Please check if you have verified your email?"
      };
    }
  }

  @Post("/change-password")
  async changePassword(@Body() payload: any): Promise<ResponseInterface<any>> {
    const { username, password, newPassword, newPasswordConfirm } = payload;
    const userDB: any = await this.userSvc.getUserByUsername(username);
    const isPass = compareSync(password, userDB.password);
    if (isPass) {
      if (newPassword === newPasswordConfirm) {
        const passHashed = hashSync(newPassword, 12);

        const userUpdate: any = {
          password: passHashed
        };

        const userJWT = await this.userSvc.updateUser(userDB._id, userUpdate);
        const jwt = sign({ userJWT }, process.env.JWT_KEY);

        return {
          data: {
            token: jwt
          }
        };
      } else {
        return {
          errors: "Password not same !!!"
        };
      }
    } else {
      return {
        errors: "Change Password fails"
      };
    }
  }

  @Post("/forget-password")
  async forgetPassword(@Body() payload: any): Promise<ResponseInterface<any>> {
    const { email } = payload;
    const userDB: any = await this.userSvc.getUserByEmail(email);
    if (userDB) {
      const tokenReset =
        Math.floor(1000 + Math.random() * 9000).toString() +
        encode("APP-MUSIC") +
        "." +
        encode(userDB.username);
      try {
        await this.mailSvc.sendResetPassword(userDB.name, email, tokenReset);
        return {
          data: {
            msg: "Please check your email to reset your password !!!"
          }
        };
      } catch (e) {
        return {
          errors: [ e ]
        };
      }
    } else {
      return {
        errors: "Email does not exist !!!"
      };
    }
  }

  @Get("/reset-password/:token")
  async getDataReset(@Param() params: any): Promise<ResponseInterface<any>> {
    const isUsername = decode(params.token.split(".").pop());
    const userDB: any = await this.userSvc.getUserByUsername(isUsername);
    if (userDB) {
      return {
        data: {
          user: userDB
        }
      };
    } else {
      return {
        data: {
          msg: "DEO CO MAIL Nay !!! Mày nhập cái quái dị dậy, aassssss triết tiệt :<"
        }
      };
    }
  }

  @Post("/upload-avatar")
  @UseInterceptors(
    FileInterceptor("file", {
      storage: diskStorage({
        destination: "./uploaded/avatars"
      })
    })
  )
  async resetPassword(
    @Body() payload: any,
    @UploadedFile() file: Express.Multer.File
  ): Promise<ResponseInterface<any>> {
    // return this.userSvc.uploadaAvatar(payload._id, {
    // 	path: file.path,
    // 	filename: file.originalname,
    // 	mimetype: file.mimetype
    //   });
    return {
      data: {
        msg: "DEO CO MAIL Nay !!! Mày nhập cái quái dị dậy, aassssss triết tiệt :<"
      }
    };
  }
}
