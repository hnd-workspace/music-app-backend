import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { UserModel, UserSchema } from "../../../schemas/user.schema";
import { MailService } from "../../../services/mail.service";
import { UserService } from "../../../services/user.service";
import { AuthController } from "./auth.controller";

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: UserModel.name,
        schema: UserSchema
      }
    ])
  ],
  controllers: [ AuthController ],
  providers: [ UserService, MailService ]
})
export class AuthModule {
}
