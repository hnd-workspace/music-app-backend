import { Controller, Get, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { I18nService } from "nestjs-i18n";

@Controller()
@UseGuards(AuthGuard("jwt"))
export class PlaylistController {
  constructor(private readonly i18nSvc: I18nService) {
  }

  @Get("/")
  private async index() {
    return {
      data: {
        test: await this.i18nSvc.translate("CORE.HELLO_WORLD")
      }
    };
  }
}
