import { Module } from "@nestjs/common";
import { PlaylistController } from "./playlist.controller";

@Module({
  imports: [],
  controllers: [ PlaylistController ],
  providers: []
})
export class PlaylistModule {
}
