import { Module } from "@nestjs/common";
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "./modules/shared/shared.module";

@Module({
  imports: [
    SharedModule,
    AppRoutingModule
  ]
})

export class AppModule {
}
