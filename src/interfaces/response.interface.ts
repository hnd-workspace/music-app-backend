export interface ResponseInterface<T> {
  data?: T;
  messages?: string | Array<string>;
  errors?: string | Array<string>;
}