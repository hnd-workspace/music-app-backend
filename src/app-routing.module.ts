import { Module } from "@nestjs/common";
import { RouterModule, Routes } from "@nestjs/core";
import { ApiModule } from "./modules/api/api.module";

import { AuthModule } from "./modules/api/auth/auth.module";
import { PlaylistModule } from "./modules/api/playlist/playlist.module";

const routes: Routes = [
  {
    path: "api",
    children: [
      {
        path: "auth",
        module: AuthModule
      },
      {
        path: "playlist",
        module: PlaylistModule
      }
    ]
  }
];

@Module({
  imports: [
    ApiModule,
    RouterModule.register(routes)
  ],
  providers: []
})

export class AppRoutingModule {
}
